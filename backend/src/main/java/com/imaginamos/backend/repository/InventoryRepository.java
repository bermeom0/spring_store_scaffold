package com.imaginamos.backend.repository;

import com.imaginamos.backend.model.Inventory;
import com.imaginamos.backend.model.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Integer> {

    @Query("SELECT i FROM Inventory i WHERE i.store.id = ?1 and i.product.id= ?2")
    Inventory findByKey(int store_id, int product_id);
}

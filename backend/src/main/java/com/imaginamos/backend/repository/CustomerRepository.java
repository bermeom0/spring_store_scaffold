package com.imaginamos.backend.repository;

import com.imaginamos.backend.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    @Query("SELECT c FROM Customer c WHERE c.username = ?1")
    Customer findByUsername(String username);

    @Query("SELECT c FROM Customer c WHERE c.username = ?1 OR c.email = ?2")
    List<Customer> findByUsernameOrEmail(String username, String email);
}

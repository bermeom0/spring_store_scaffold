package com.imaginamos.backend.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "store")
public class Store extends AbstractDTOObject {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int ID;

    private String name;
    private int open;
    private int close;
    private String address;

    @OneToMany(mappedBy = "store")
    @JsonIgnore
    private List<Inventory> inventories;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOpen() {
        return open;
    }

    public void setOpen(int open) {
        this.open = open;
    }

    public int getClose() {
        return close;
    }

    public void setClose(int close) {
        this.close = close;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Inventory> getInventories() {
        return inventories;
    }

    public void setInventories(List<Inventory> inventories) {
        this.inventories = inventories;
    }

    public void update (Store newStore){
        this.name = newStore.getName();
        this.open = newStore.getOpen();
        this.close = newStore.getClose();
        this.address = newStore.getAddress();
    }
}

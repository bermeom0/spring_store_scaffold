package com.imaginamos.backend.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "customer")
public class Customer extends AbstractDTOObject {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int ID;

    private String name;
    private int age;
    private int document;
    private String email;
    private String username;
    private String password;

    @OneToMany(mappedBy = "customer")
    private List<Bill> bills;

    public Customer() {
    }

    public Customer(String name, int age, int document, String email, String username, String password, List<Bill> bills) {
        this.name = name;
        this.age = age;
        this.document = document;
        this.email = email;
        this.username = username;
        this.password = password;
        this.bills = bills;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getDocument() {
        return document;
    }

    public void setDocument(int document) {
        this.document = document;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Bill> getBills() {
        return bills;
    }

    public void setBills(List<Bill> bills) {
        this.bills = bills;
    }

    public void update (Customer newCustomer){
        this.name = newCustomer.getName();
        this.age = newCustomer.getAge();
        this.document = newCustomer.getDocument();
        this.email = newCustomer.getEmail();
        this.username = newCustomer.getUsername();
        this.password = newCustomer.getPassword();
    }
}

package com.imaginamos.backend.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "inventory")
public class Inventory extends AbstractDTOObject {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int ID;

    private int available;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_ID", nullable = false)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_ID", nullable = false)
    private Store store;

    @OneToMany(mappedBy = "inventory", orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<BillItem> billItems;

    public Inventory() {
    }

    public Inventory(int available, Product product, Store store, List<BillItem> billItems) {
        this.available = available;
        this.product = product;
        this.store = store;
        this.billItems = billItems;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    @JsonIgnore
    public List<BillItem> getBillItems() {
        return billItems;
    }

    public void setBillItems(List<BillItem> billItems) {
        this.billItems = billItems;
    }
}


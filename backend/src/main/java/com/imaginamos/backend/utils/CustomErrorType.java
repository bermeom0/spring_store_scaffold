package com.imaginamos.backend.utils;

public class CustomErrorType {
    private String errorType;

    public CustomErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorType() {
        return errorType;
    }
}

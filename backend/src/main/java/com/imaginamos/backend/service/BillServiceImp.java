package com.imaginamos.backend.service;


import com.imaginamos.backend.controller.BillController;
import com.imaginamos.backend.model.Bill;
import com.imaginamos.backend.model.BillItem;
import com.imaginamos.backend.model.Customer;
import com.imaginamos.backend.repository.BillRepository;
import com.imaginamos.backend.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("billService")
@Transactional
public class BillServiceImp implements BillService {

    @Autowired
    private BillRepository billRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Bill findById(int id) {
        return billRepository.findOne(id);
    }

    @Override
    public void saveBill(Bill bill) {
        bill.setCustomer(customerRepository.getOne(bill.getCustomer().getID()));
        billRepository.save(bill);
    }

    @Override
    public void updateBill(Bill bill) {
        billRepository.save(bill);
    }

    @Override
    public void deleteBillById(int id) {
        billRepository.delete(id);
    }

    @Override
    public void deleteAllBills() {
        billRepository.deleteAll();
    }

    @Override
    public List<Bill> findAllBills() {
        return billRepository.findAll();
    }

    @Override
    public boolean isBillExist(Bill bill) {
        if (billRepository.findOne(bill.getID()) == null)
            return false;
        return true;
    }
}

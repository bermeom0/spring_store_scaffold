package com.imaginamos.backend.service;

import com.imaginamos.backend.model.Customer;

import java.util.List;

public interface AuthenticationService {
    Customer logIn(String username, String password);
}

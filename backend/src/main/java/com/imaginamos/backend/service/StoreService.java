package com.imaginamos.backend.service;

import com.imaginamos.backend.model.Product;
import com.imaginamos.backend.model.Store;

import java.util.List;

public interface StoreService {
    Store findById(int id);

    Store findByName(String name);

    void saveStore(Store store);

    void updateStore(Store store);

    void deleteStoreById(int id);

    void deleteAllStores();

    List<Store> findAllStores();

    boolean isStoreExist(Store store);
}

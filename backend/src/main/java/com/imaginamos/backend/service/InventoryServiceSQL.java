package com.imaginamos.backend.service;


import com.imaginamos.backend.model.Customer;
import com.imaginamos.backend.model.Inventory;
import com.imaginamos.backend.repository.CustomerRepository;
import com.imaginamos.backend.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("inventoryService")
@Transactional
public class InventoryServiceSQL implements InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    @Override
    public void saveInventory(Inventory inventory) {
        inventoryRepository.save(inventory);
    }

    @Override
    public List<Inventory> findAllInventories() {
        return inventoryRepository.findAll();
    }

    @Override
    public boolean isInventoryExist(Inventory inventory) {
        if(inventoryRepository.findByKey(inventory.getStore().getID(), inventory.getProduct().getID()) == null)
            return false;
        return true;
    }
}

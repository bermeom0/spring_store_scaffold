package com.imaginamos.backend.service;


import com.imaginamos.backend.model.Product;
import com.imaginamos.backend.model.Store;
import com.imaginamos.backend.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("storeService")
@Transactional
public class StoreServiceSQL implements StoreService{

    @Autowired
    private StoreRepository storeRepository;

    @Override
    public Store findById(int id) {
        return storeRepository.findOne(id);
    }

    @Override
    public Store findByName(String name) {
        return null;
    }

    @Override
    public void saveStore(Store store) {
        storeRepository.save(store);
    }

    @Override
    public void updateStore(Store store) {
        storeRepository.save(store);
    }

    @Override
    public void deleteStoreById(int id) {
        storeRepository.delete(id);
    }

    @Override
    public void deleteAllStores() {

    }

    @Override
    public List<Store> findAllStores() {
        return storeRepository.findAll();
    }

    @Override
    public boolean isStoreExist(Store store) {
        return false;
    }
}

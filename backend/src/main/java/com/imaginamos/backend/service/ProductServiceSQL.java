package com.imaginamos.backend.service;


import com.imaginamos.backend.model.Product;
import com.imaginamos.backend.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("productService")
@Transactional
public class ProductServiceSQL implements ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product findById(int id) {
        return productRepository.findOne(id);
    }

    @Override
    public Product findByName(String name) {
        return null;
    }

    @Override
    public void saveProduct(Product product) {
        productRepository.save(product);
    }

    @Override
    public void updateProduct(Product product) {
        productRepository.save(product);
    }

    @Override
    public void deleteProductById(int id) {
        productRepository.delete(id);
    }

    @Override
    public void deleteAllProducts() {

    }

    @Override
    public List<Product> findAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public boolean isProductExist(Product product) {
        return false;
    }
}

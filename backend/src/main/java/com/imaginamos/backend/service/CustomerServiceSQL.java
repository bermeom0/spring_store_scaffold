package com.imaginamos.backend.service;


import com.imaginamos.backend.model.Customer;
import com.imaginamos.backend.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("customerService")
@Transactional
public class CustomerServiceSQL implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer findById(int id) {
        return customerRepository.findOne(id);
    }

    @Override
    public Customer findByUserName(String username) {
        return customerRepository.findByUsername(username);
    }

    @Override
    public void saveCustomer(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public void updateCustomer(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public void deleteCustomerById(int id) {
        customerRepository.delete(id);
    }

    @Override
    public void deleteAllCustomers() {

    }

    @Override
    public List<Customer> findAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public boolean isCustomerExist(Customer customerToAdd) {
        List<Customer> customers = customerRepository.findByUsernameOrEmail(customerToAdd.getUsername(), customerToAdd.getEmail());
        if(customers.isEmpty())
            return false;
        return true;
    }
}

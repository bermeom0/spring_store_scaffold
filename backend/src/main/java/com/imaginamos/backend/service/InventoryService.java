package com.imaginamos.backend.service;

import com.imaginamos.backend.model.Customer;
import com.imaginamos.backend.model.Inventory;

import java.util.List;

public interface InventoryService {
    void saveInventory(Inventory inventory);
    List<Inventory> findAllInventories();

    boolean isInventoryExist(Inventory inventory);
}

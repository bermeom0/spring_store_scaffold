package com.imaginamos.backend.service;

import com.imaginamos.backend.model.Product;

import java.util.List;

public interface ProductService {
    Product findById(int id);

    Product findByName(String name);

    void saveProduct(Product product);

    void updateProduct(Product product);

    void deleteProductById(int id);

    void deleteAllProducts();

    List<Product> findAllProducts();

    boolean isProductExist(Product product);
}

package com.imaginamos.backend.service;

import com.imaginamos.backend.model.Customer;

import java.util.List;

public interface CustomerService {
    Customer findById(int id);

    Customer findByUserName(String username);

    void saveCustomer(Customer customer);

    void updateCustomer(Customer customer);

    void deleteCustomerById(int id);

    void deleteAllCustomers();

    List<Customer> findAllCustomers();

    boolean isCustomerExist(Customer customer);
}

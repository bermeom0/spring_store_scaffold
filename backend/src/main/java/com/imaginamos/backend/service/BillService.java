package com.imaginamos.backend.service;

import com.imaginamos.backend.model.Bill;

import java.util.List;

public interface BillService {

    Bill findById(int id);

    void saveBill(Bill bill);

    void updateBill(Bill bill);

    void deleteBillById(int id);

    void deleteAllBills();

    List<Bill> findAllBills();

    boolean isBillExist(Bill bill);
}

package com.imaginamos.backend.service;


import com.imaginamos.backend.model.Customer;
import com.imaginamos.backend.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("authenticationService")
@Transactional
public class AuthenticationServiceImp implements AuthenticationService {

    @Autowired
    CustomerService customerService;

    @Override
    public Customer logIn(String username, String password) {
        Customer customer = customerService.findByUserName(username);
        if(customer == null || !customer.getPassword().equals(password))
            customer = null;
        return customer;
    }
}

package com.imaginamos.backend.controller;

import com.imaginamos.backend.model.AuthenticationData;
import com.imaginamos.backend.model.Customer;
import com.imaginamos.backend.service.AuthenticationService;
import com.imaginamos.backend.service.CustomerService;
import com.imaginamos.backend.utils.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AuthenticationController {

    public static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired
    AuthenticationService authenticationService;

    @RequestMapping(value = "/authentication/login/", method = RequestMethod.POST)
    public ResponseEntity<?> logIn(@RequestBody AuthenticationData authenticationData){
        logger.info("Doing logIn of customer");
        Customer customer = authenticationService.logIn(authenticationData.getUsername(), authenticationData.getPassword());
        if(customer == null){
            logger.error("Unable to logIn. Invalid Username or Password");
            return new ResponseEntity(new CustomErrorType("Unable to logIn. Invalid Username or Password"), HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<Customer>(customer, HttpStatus.OK);
    }

}

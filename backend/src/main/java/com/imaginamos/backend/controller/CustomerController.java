package com.imaginamos.backend.controller;

import com.imaginamos.backend.model.Customer;
import com.imaginamos.backend.service.CustomerService;
import com.imaginamos.backend.utils.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomerController {

    public static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    CustomerService customerService;

    @RequestMapping(value = "/customer/", method = RequestMethod.GET)
    public ResponseEntity<List<Customer>> listAllCustomers(){
        logger.info(("Fetching all customers"));
        List<Customer> customers = customerService.findAllCustomers();
        if(customers.isEmpty()) {
            logger.error(("No customers available"));
            return new ResponseEntity(customers, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @RequestMapping(value = "/customer/{id}", method = RequestMethod.GET)
    public ResponseEntity<Customer> getCustomer(@PathVariable("id") int id){
        logger.info("Fetching customer with id {}", id);
        Customer customer = customerService.findById(id);
        if(customer == null){
            logger.error("Fetching customer with id {} not found", id);
            return new ResponseEntity(new CustomErrorType("Customer with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @RequestMapping(value="/customer/", method = RequestMethod.POST)
    public ResponseEntity<?> createCustomer(@RequestBody Customer customer, UriComponentsBuilder builder){
        logger.info("Creating customer : {}", customer.getName());
        if(customerService.isCustomerExist(customer)){
            logger.error("Unable to create customer. A customer with email " + customer.getEmail() + " or document " + customer.getDocument() + " already exist");
            return new ResponseEntity(new CustomErrorType("Unable to create customer. A customer with email " +
                    customer.getEmail() + " or document " + customer.getDocument() + " already exist"), HttpStatus.CONFLICT);
        }
        customerService.saveCustomer(customer);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/customer/{id}").buildAndExpand(customer.getID()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value="/customer/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateCustomer(@PathVariable("id") int id, @RequestBody Customer newCustomer){
        logger.info("Updating customer with id {}", id);
        Customer customer = customerService.findById(id);
        if(customer == null){
            logger.error("Unable to update. Customer with id " + newCustomer.getID() + " not found.");
            return new ResponseEntity(new CustomErrorType("Unable to update. Customer with id " +
                    newCustomer.getID() + " not found."), HttpStatus.NOT_FOUND);
        }
        customer.update(newCustomer);
        customerService.updateCustomer(customer);
        return new ResponseEntity<Customer>(customer,HttpStatus.OK);
    }

    @RequestMapping(value="customer/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteCustomer(@PathVariable("id") int id){
        logger.info("Deleting customer with id {}", id);
        Customer customer = customerService.findById(id);
        if(customer == null){
            logger.error("Unable to delete. Customer with id " + id + " not found.");
            return new ResponseEntity(new CustomErrorType("Unable to delete. Customer with id " +
                    id + " not found."), HttpStatus.NOT_FOUND);
        }
        customerService.deleteCustomerById(id);
        return new ResponseEntity<Customer>(HttpStatus.OK);
    }
}

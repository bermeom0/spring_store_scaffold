package com.imaginamos.backend.controller;

import com.imaginamos.backend.model.Bill;
import com.imaginamos.backend.model.Inventory;
import com.imaginamos.backend.service.BillService;
import com.imaginamos.backend.service.InventoryService;
import com.imaginamos.backend.utils.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/api")
public class InventoryController {

    public static final Logger logger = LoggerFactory.getLogger(InventoryController.class);

    @Autowired
    InventoryService inventoryService;

    @RequestMapping(value = "/inventory/", method = RequestMethod.GET)
    public ResponseEntity<List<Inventory>> listAllInventories(){
        logger.info(("Fetching all inventories"));
        List<Inventory> inventories = inventoryService.findAllInventories();
        if(inventories.isEmpty()) {
            logger.error("No inventories available");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(inventories, HttpStatus.OK);
    }


    @RequestMapping(value="/inventory/", method = RequestMethod.POST)
    public ResponseEntity<?> createInventory(@RequestBody Inventory inventory, UriComponentsBuilder builder){
        logger.info("Creating inventory: {}", inventory);
        if(inventoryService.isInventoryExist(inventory)){
            logger.error("Unable to create inventory. A inventory with id " + inventory.getID() +  " already exist");
            return new ResponseEntity(new CustomErrorType("Unable to create inventory. A inventory with product id " +
                    inventory.getProduct().getID() +  " and store id "+ inventory.getStore().getID() + " already exist"),
                    HttpStatus.CONFLICT);
        }
        inventoryService.saveInventory(inventory);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/inventory/{id}").buildAndExpand(inventory.getID()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }
}

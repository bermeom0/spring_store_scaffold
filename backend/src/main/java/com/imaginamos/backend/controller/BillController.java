package com.imaginamos.backend.controller;

import com.imaginamos.backend.model.Bill;
import com.imaginamos.backend.model.Store;
import com.imaginamos.backend.service.BillService;
import com.imaginamos.backend.service.StoreService;
import com.imaginamos.backend.utils.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BillController {

    public static final Logger logger = LoggerFactory.getLogger(BillController.class);

    @Autowired
    BillService billService;

    @RequestMapping(value = "/bill/", method = RequestMethod.GET)
    public ResponseEntity<List<Bill>> listAllBills(){
        logger.info(("Fetching all bills"));
        List<Bill> bills = billService.findAllBills();
        if(bills.isEmpty()) {
            logger.error("No bills available");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(bills, HttpStatus.OK);
    }

    @RequestMapping(value = "/bill/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getBill(@PathVariable("id") int id){
        logger.info("Fetching bill with id {}", id);
        Bill bill = billService.findById(id);
        if(bill == null){
            logger.error("Fetching bill with id {} not found", id);
            return new ResponseEntity(new CustomErrorType("Bill with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Bill>(bill, HttpStatus.OK);
    }

    @RequestMapping(value="/bill/", method = RequestMethod.POST)
    public ResponseEntity<?> createBill(@RequestBody Bill bill, UriComponentsBuilder builder){
        logger.info("Creating bill: {}", bill);
        if(billService.isBillExist(bill)){
            logger.error("Unable to create bill. A bill with id " + bill.getID() +  " already exist");
            return new ResponseEntity(new CustomErrorType("Unable to create bill. A bill with id " +
                    bill.getID() +  " already exist"), HttpStatus.CONFLICT);
        }
        billService.saveBill(bill);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/bill/{id}").buildAndExpand(bill.getID()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value="/bill/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateBill(@PathVariable("id") int id, @RequestBody Bill newBill){
        logger.info("Updating bill with id {}", id);
        Bill bill = billService.findById(id);
        if(bill == null){
            logger.error("Unable to update. Bill with id " + newBill.getID() + " not found.");
            return new ResponseEntity(new CustomErrorType("Unable to update. Bill with id " +
                    newBill.getID() + " not found."), HttpStatus.NOT_FOUND);
        }
        bill.update(newBill);
        billService.updateBill(bill);
        return new ResponseEntity<Bill>(bill,HttpStatus.OK);
    }

    @RequestMapping(value="bill/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteBill(@PathVariable("id") int id){
        logger.info("Deleting bill with id {}", id);
        Bill bill = billService.findById(id);
        if(bill == null){
            logger.error("Unable to delete. Bill with id " + id + " not found.");
            return new ResponseEntity(new CustomErrorType("Unable to delete. Bill with id " +
                    id + " not found."), HttpStatus.NOT_FOUND);
        }
        billService.deleteBillById(id);
        return new ResponseEntity<Bill>(HttpStatus.OK);
    }

    @RequestMapping(value="bill/", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteBills(){
        logger.info("Deleting all bills");
        billService.deleteAllBills();
        return new ResponseEntity<Bill>(HttpStatus.OK);
    }
}

package com.imaginamos.backend.controller;

import com.imaginamos.backend.model.Customer;
import com.imaginamos.backend.model.Product;
import com.imaginamos.backend.service.CustomerService;
import com.imaginamos.backend.service.ProductService;
import com.imaginamos.backend.utils.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductController {

    public static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/product/", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> listAllProducts(){
        logger.info(("Fetching all products"));
        List<Product> products = productService.findAllProducts();
        if(products.isEmpty()) {
            logger.error(("No products available"));
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public ResponseEntity<Product> getProduct(@PathVariable("id") int id){
        logger.info("Fetching product with id {}", id);
        Product product= productService.findById(id);
        if(product == null){
            logger.error("Fetching product with id {} not found", id);
            return new ResponseEntity(new CustomErrorType("Product with id " + id
                    + " not found"), HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @RequestMapping(value="/product/", method = RequestMethod.POST)
    public ResponseEntity<?> createProduct(@RequestBody Product product, UriComponentsBuilder builder){
        logger.info("Creating product: {}", product);
        if(productService.isProductExist(product)){
            logger.error("Unable to create product. A product with barcode " + product.getBarcode() +  " already exist");
            return new ResponseEntity(new CustomErrorType("Unable to create product. A product with barcode " +
                    product.getBarcode() +  " already exist"), HttpStatus.CONFLICT);
        }
        productService.saveProduct(product);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/product/{id}").buildAndExpand(product.getID()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value="/product/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") int id, @RequestBody Product newProduct){
        logger.info("Updating product with id {}", id);
        Product product = productService.findById(id);
        if(product == null){
            logger.error("Unable to update. Product with id " + newProduct.getID() + " not found.");
            return new ResponseEntity(new CustomErrorType("Unable to update. Product with id " +
                    newProduct.getID() + " not found."), HttpStatus.NOT_FOUND);
        }
        product.update(newProduct);
        productService.updateProduct(product);
        return new ResponseEntity<Product>(product,HttpStatus.OK);
    }

    @RequestMapping(value="product/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") int id){
        logger.info("Deleting product with id {}", id);
        Product product = productService.findById(id);
        if(product == null){
            logger.error("Unable to delete. Product with id " + id + " not found.");
            return new ResponseEntity(new CustomErrorType("Unable to delete. Product with id " +
                    id + " not found."), HttpStatus.NOT_FOUND);
        }
        productService.deleteProductById(id);
        return new ResponseEntity<Product>(HttpStatus.OK);
    }
}

package com.imaginamos.backend.controller;

import com.imaginamos.backend.model.Product;
import com.imaginamos.backend.model.Store;
import com.imaginamos.backend.service.ProductService;
import com.imaginamos.backend.service.StoreService;
import com.imaginamos.backend.utils.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StoreController {

    public static final Logger logger = LoggerFactory.getLogger(StoreController.class);

    @Autowired
    StoreService storeService;

    @RequestMapping(value = "/store/", method = RequestMethod.GET)
    public ResponseEntity<List<Store>> listAllStores(){
        logger.info(("Fetching all stores"));
        List<Store> stores = storeService.findAllStores();
        if(stores.isEmpty()) {
            logger.error(("No stores available"));
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(stores, HttpStatus.OK);
    }

    @RequestMapping(value = "/store/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getStore(@PathVariable("id") int id){
        logger.info("Fetching store with id {}", id);
        Store store = storeService.findById(id);
        if(store == null){
            logger.error("Fetching store with id {} not found", id);
            return new ResponseEntity(new CustomErrorType("Store with id " + id
                    + " not found"), HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Store>(store, HttpStatus.OK);
    }

    @RequestMapping(value="/store/", method = RequestMethod.POST)
    public ResponseEntity<?> createStore(@RequestBody Store store, UriComponentsBuilder builder){
        logger.info("Creating store: {}", store);
        if(storeService.isStoreExist(store)){
            logger.error("Unable to create store. A store with address " + store.getAddress() +  " already exist");
            return new ResponseEntity(new CustomErrorType("Unable to create store. A store with address" +
                    store.getAddress() +  " already exist"), HttpStatus.CONFLICT);
        }
        storeService.saveStore(store);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/store/{id}").buildAndExpand(store.getID()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value="/store/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") int id, @RequestBody Store newStore){
        logger.info("Updating store with id {}", id);
        Store store = storeService.findById(id);
        if(store == null){
            logger.error("Unable to update. Store with id " + newStore.getID() + " not found.");
            return new ResponseEntity(new CustomErrorType("Unable to update. Store with id " +
                    newStore.getID() + " not found."), HttpStatus.NOT_FOUND);
        }
        store.update(newStore);
        storeService.updateStore(store);
        return new ResponseEntity<Store>(store,HttpStatus.OK);
    }

    @RequestMapping(value="store/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") int id){
        logger.info("Deleting store with id {}", id);
        Store store = storeService.findById(id);
        if(store == null){
            logger.error("Unable to delete. Store with id " + id + " not found.");
            return new ResponseEntity(new CustomErrorType("Unable to delete. Store with id " +
                    id + " not found."), HttpStatus.NOT_FOUND);
        }
        storeService.deleteStoreById(id);
        return new ResponseEntity<Store>(HttpStatus.OK);
    }
}

package com.imaginamos.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.imaginamos.backend.BackendApplication;
import com.imaginamos.backend.model.*;
import com.imaginamos.backend.repository.*;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackendApplication.class)
@WebAppConfiguration
public class BillControllerIntegrationTest {
    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;

    private final ObjectMapper mapper = new ObjectMapper();

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Value("classpath:/mocks/controllers/bill/newStore.json")
    private File newStoreJson;

    @Value("classpath:/mocks/controllers/bill/newProduct.json")
    private File newProductJson;

    @Value("classpath:/mocks/controllers/bill/newInventory.json")
    private File newInventoryJson;

    @Value("classpath:/mocks/controllers/bill/newCustomer.json")
    private File newCustomerJson;

    @Value("classpath:/mocks/controllers/bill/newBillRequest.json")
    private File newBillRequestJson;

    @Value("classpath:/mocks/controllers/bill/newBillResponse.json")
    private File newBillResponseJson;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private BillRepository billRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        int storeID = storeRepository.save(mapper.readValue(newStoreJson, Store.class)).getID();
        int productID = productRepository.save(mapper.readValue(newProductJson, Product.class)).getID();
        Inventory inventory = mapper.readValue(newInventoryJson, Inventory.class);
        inventory.getStore().setID(storeID);
        inventory.getProduct().setID(productID);
        int inventoryID = inventoryRepository.save(inventory).getID();
        int customerID = customerRepository.save(mapper.readValue(newCustomerJson, Customer.class)).getID();
        Bill bill = mapper.readValue(newBillRequestJson, Bill.class);
        bill.getCustomer().setID(customerID);
        bill.getBillItems().stream().forEach(billItem -> billItem.getInventory().setID(inventoryID));
        billRepository.save(bill);
    }

    @After
    public void delete() throws Exception {
        inventoryRepository.deleteAll();
        billRepository.deleteAll();
        storeRepository.deleteAll();
        productRepository.deleteAll();
        customerRepository.deleteAll();
    }

    @Test
    public void findExistingBill() throws Exception{
        mockMvc.perform(get("/api/bill/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(FileUtils.readFileToString(newBillResponseJson, "UTF-8")));
    }

    @Test
    public void findNonExistingBill() throws  Exception{
        mockMvc.perform(get("/api/bill/10"))
                .andExpect(status().isNotFound());
    }
}

package com.imaginamos.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.imaginamos.backend.BackendApplication;
import com.imaginamos.backend.model.Customer;
import com.imaginamos.backend.repository.CustomerRepository;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackendApplication.class)
@WebAppConfiguration
public class AuthenticationControllerIntegrationTest {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;

    private final ObjectMapper mapper = new ObjectMapper();

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Value("classpath:/mocks/controllers/authentication/newCustomer.json")
    private File newCustomerJson;

    @Value("classpath:/mocks/controllers/authentication/logInSuccessRequest.json")
    private File logInSuccessRequestJson;

    @Value("classpath:/mocks/controllers/authentication/logInSuccessResponse.json")
    private File logInSuccessResponseJson;

    @Value("classpath:/mocks/controllers/authentication/logInFailRequest.json")
    private File logInFailRequestJson;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private CustomerRepository customerRepository;

    private int customerID;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        this.customerID = customerRepository.save(mapper.readValue(newCustomerJson, Customer.class)).getID();
    }

    @After
    public void delete() throws Exception {
        customerRepository.delete(customerID);
    }

    @Test
    public void logInSuccessTest() throws Exception{
        mockMvc.perform(post("/api/authentication/login/")
                .contentType(contentType)
                .content(FileUtils.readFileToString(logInSuccessRequestJson, "UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().json(FileUtils.readFileToString(logInSuccessResponseJson, "UTF-8")));
    }

    @Test
    public void logInFailTest() throws Exception{
        mockMvc.perform(post("/api/authentication/login/")
                .contentType(contentType)
                .content(FileUtils.readFileToString(logInFailRequestJson, "UTF-8")))
                .andExpect(status().isForbidden());
    }
}

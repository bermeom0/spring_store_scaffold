# **Test Imaginamos**

En este repositorio se puede encontrar toda la documentación de la prueba para ingeniero backend desarrollada para imaginamos.com.

**Tabla de contenido**
 
[TOC]

## **Codigo Backend**

El codigo fue desarrollado con Spring Boot, implementando servicios para cada entidad solicitada y controladores acordes a los requerimientos del problema.

### **Servicios**
   - CustomerService: CRUD de clientes.
   - ProductService: CRUD de productos.
   - StoreService: CRUD de tiendas.
   - BillService: CRUD de facturas.
   - InventoryService: CRUD de inventario de productos.
   - AuthenticationService: Servicio de LogIn.

A continuación se detallan los servicios mas importantes:

 **Crear Factura:**

	Endpoint: http://34.193.128.68:8083/api/bill/
	Method: POST
	Content-type: application/json
	body: {
        "customer": {
	         "id": Integer
        },
        "billItems": [
			{
				"quantity": Integer,
				"inventory":{
					"id": Integer
				}
			}
		]	
	}
		
 **LogIn**:
	
	Endpoint:
	http://34.193.128.68:8083/api/authentication/login
	Method: POST
	Content-type: application/json
	body: {
		"username": String,
		"password": String
	}

----------


### **Despliegue**

El código puede utilizar el tomcat embebido de springboot, para desplegar seguir los siguientes pasos: 

Compilar el código.

```sh
$ mvn clean install
```

Desplegar Jar.

```sh
$ java -jar backend-0.0.1-SNAPSHOT.jar
```


----------

### **AWS EC2**
Por facilidad se desplegó el aplicativo en una instancia RHEL7 en el proovedor de servicios AWS.

> **Acceso**
```
Ip: http://34.193.128.68
Puerto: 8083
```
### **Testing**

Se realizaron casos de prueba de integración para los dos controladores mas importantes utilizando una base de datos en memoria H2 y MockMvc.

- CustomerController
	-  FindExistingCustomer
	-  FindNonExistingCustomer
	-  DuplicatedCustomer
	
- BillController
	- FindExistingBill
	- FindNonExistingBill

- AuthenticationController
	- LogInSuccess
	- LogInFail 

----------
## **Base de datos**

El modelo relacional y el script de base de dat
os pueden encontrarse en la carpeta ***database***.

### **SQL**
#### **Entidades**
   >**Customer:** Clientes.
   
    {
        id: Integer,
        name: String,
        age: Integer,
        document: Integer,
        email: String,
        username: String,
        password: String,
        bills: Array<Bll>
    }
 
   > **Product:** Productos.
	   
	   {
        id: Integer,
        name: String, 
        description: String,
        price: Double,
        barcode: Integer,
        inventories: Array<Inventory>
    }
   > **Store:** Tiendas.

    {
        int: Integer,
        name: String,
        open: Integer,
        close: Integer,
        address: String,
        inventories: Array<Inventory>
    }
   > **Inventory:** Relación entre tiendas y productos, la cual indica la cantidad de productos disponibles en una tienda determinada.

    {
        id: Integer,
        available: Integer,
        product: Product,
        store: Store,
        billItems: Array<BillItem>
    }
   > **Bill:** Factura.

    {
        id: Integer,
        customer: Customer,
        billItems: Array<BillItem>
    }
  > **BillItem:** Relación entre factura y productos, la cual indica la cantidad de productos que se vendieron.

    {
        id: Integer,
        quantity: Integer,
        bill: Bill,
        inventory: Inventory
    }


#### **Google SQL**

Por facilidad se desplegó una base de datos MySQL en el proovedor de servicios Google SQL. 

> **Acceso**
```
Ip: 104.198.44.30
Puerto: 3306
Instancia: mydb
Usuario: root
Contraseña: imaginamos
```
----------
### **NoSQL**

Para la utilización de un modelo no relacional se ve la necesidad de crear los siguientes esquemas:

- **Store**

		{
			"name": String,
			"open": Integer,
			"close": Integer,
			"address": String,
			"products":   [
			                     {
				                     "product_id": Integer         
			                     }
			                    ]
		}

- **Product**
		
		{
			"id": Integer,
			"name": String,
			"description": String,
			"price": Double,
			"barcode" : Integer
		}

- **Inventory**

		{
			"id": product_id,
			"items": [
				{
					"store_id": Integer,
					"quantity": Integer
				}
			] 
		}

- **Customer**

		{
			"id": Integer,
			"name": String,
			"age": Integer,
			"document": Integer,
			"email": String,
			"username": String,
			"password": String,
			"products": Array<Product>
		}

- **Bill**
		
		{
			"id" : Integer,
			"customer_id": Customer_id,
			"items":[
					{
						"id": Product_id,
						"quantity": Integer
					}
			]
		}